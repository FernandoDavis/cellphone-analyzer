package p1MainClasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.AbstractMap;
import java.util.ArrayList;

import dataGenerator.DataGenerator;
/**
 * ExperimentController class to test the run time of the strategies
 * developed during part1 of this project
 * 
 * @author Irving Lazu, 802-15-3736, Section 030
 * @author Fernando Rivera, 840-15-2011, Section 030
 * 
 * NOTE: based on code developed by @author pedroirivera-vega for the sorting lab
 */
public class ExperimentController{
	
	private int telComp,			   // telephone companies
				crimeEv,		       // crime events
				initialSize,           // initial size to be tested
				repetitionsPerSize,    // experimental repetitions per size
				incrementalSizeStep,   // change of sizes (size delta)
				finalSize;             // last size to be tested
	
	private ArrayList<StrategiesTimeCollection<Integer>> resultsPerStrategy; 
	// The i-th position will contain a particular strategy being tested. 
	// At the end, the i-th position will also contain a list of 
	// pairs (n, t), where t is the estimated time for size n for
	// the strategy at that position. 
	
	// Constructor initializing all the private variables
	public ExperimentController(int tc, int ce, int is, int fs, int iss, int rps) {
		telComp = tc;
		crimeEv = ce;
		initialSize = is; 
		repetitionsPerSize = rps; 
		incrementalSizeStep = iss; 
		finalSize = fs; 
		resultsPerStrategy = new ArrayList<>(); 
	}
	
	/**
	 * This method simply adds the strategy to a list
	 * 
	 * @param strategy, strategy to be tested
	 */
	public void addStrategy(StrategiesTimeCollection<Integer> strategy) { 
		resultsPerStrategy.add(strategy); 
	}

	/**
	 * Method to test all strategies for the specified sizes and parameters
	 * 
	 * @throws CloneNotSupportedException
	 */
	public void run() throws CloneNotSupportedException { 
		if (resultsPerStrategy.isEmpty())
			throw new IllegalStateException("No strategy has been added."); 
		for (int size=initialSize; size<=finalSize; size+=incrementalSizeStep) { 
			// For each strategy, reset the corresponding variable that will be used
			// to store the sum of times that the particular strategy exhibits for
			// the current size size
			for (StrategiesTimeCollection<Integer> strategy : resultsPerStrategy) 
				strategy.resetSum();  
			
			// Run all trials for the current size. 
			for (int r = 0; r<repetitionsPerSize; r++) {
				// The following will be the common dataset to be used in the current 
				// trial by all the strategies being tested.
				Integer[][][] data = StrategiesTimeCollection.generateData(telComp, crimeEv, size);  
				
				// Apply each one of the strategies being tested using the previous 
				// dataset as input; and, for each, estimate the time
				// that the execution takes. 
				for (StrategiesTimeCollection<Integer> strategy : resultsPerStrategy) {  
					long startTime = System.nanoTime(); // time before

					strategy.runTrial(data.clone());   // run the particular strategy
					
					long endTime = System.nanoTime(); // time after

					// accumulate the estimated time to sum of times that
					// the current strategy has exhibited on trials for datasets
					// of the current size. 
					strategy.incSum((int) (endTime-startTime));
				}
			}

			for (StrategiesTimeCollection<Integer> strategy : resultsPerStrategy) { 
				strategy.add(new AbstractMap.SimpleEntry<Integer, Float>
				(size, strategy.computeAvgTime(repetitionsPerSize))); 
			}

			System.out.println(size); // current size being tested

		}
	}

	/**
	 * Method to save results from experimentation to the
	 * allResults.txt file
	 * 
	 * @throws FileNotFoundException, if file root does not exist
	 */
	public void saveResults() throws FileNotFoundException { 
		
		PrintStream out = new PrintStream(new File("experimentalResults", "allResults.txt"));
		out.print("Size");
		for (StrategiesTimeCollection<Integer> trc : resultsPerStrategy) 
			out.print("\t" + trc.getStrategyName()); 
		out.println();

		int numberOfExperiments = resultsPerStrategy.get(0).size(); 
		for (int i=0; i<numberOfExperiments; i++) {
			out.print(resultsPerStrategy.get(0).get(i).getKey());
			for (StrategiesTimeCollection<Integer> trc : resultsPerStrategy)
				out.print("\t" + trc.get(i).getValue());
			out.println(); 
		}
			
		out.close();
		
	}
}