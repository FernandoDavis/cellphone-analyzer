package p1MainClasses;

import java.io.FileNotFoundException;
import java.util.Random;

import dataGenerator.DataReader;
import interfaces.MySet;
import mySetImplementations.Set1;
import setIntersectionFinders.GenerateDataSets;
import setIntersectionFinders.P1AndP2;
import setIntersectionFinders.P3;
import setIntersectionFinders.P4;

public class Part1Main {

	public static void main(String[] args) throws FileNotFoundException {
		DataReader dataReader = new DataReader();
		try {
			Object[][][] rawData = dataReader.readDataFiles();
			GenerateDataSets dataSet = new GenerateDataSets(rawData);
			if(args.length>0) {
				switch(args[0]) {
				case("1"):
					System.out.println(new P1AndP2("1").intersectSets(dataSet.generateSets(1)).toString());
				break;
				case("2"):
					System.out.println(new P1AndP2("2").intersectSets(dataSet.generateSets(2)).toString());
				break;
				case("3"):
					System.out.println(new P3("3").intersectSets(dataSet.generateSets(3)).toString());
				break;
				case("4"):
					System.out.println(new P4("4").intersectSets(dataSet.generateSets(4)).toString());
				break;
				default:
					System.out.println("Hablamos.");
				break;
				}
			}
			else if(args.length == 0){
				System.out.println(new P1AndP2("1").intersectSets(dataSet.generateSets(1)));
				System.out.println(new P1AndP2("2").intersectSets(dataSet.generateSets(2)));
				System.out.println(new P3("3").intersectSets(dataSet.generateSets(3)));
				System.out.println(new P4("4").intersectSets(dataSet.generateSets(4)));
			}
		}
		catch(FileNotFoundException e){
			System.out.println("File not found in project.");
			try {
				throw new Exception("FileNotFoundException");
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}			
}



