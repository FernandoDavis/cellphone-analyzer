package p1MainClasses;

import java.io.FileNotFoundException;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;

import setIntersectionFinders.P1AndP2;
import setIntersectionFinders.P3;
import setIntersectionFinders.P4;
/**
 * Main method of part2 which calls and executes all necessary
 * methods from other classes (ExperimentController, 
 * StrategiesTimeCollection, etc).
 * 
 * If no arguments are provided in the command line the code will
 * execute with pre-established default parameters stored in the
 * defParms array.
 * 
 * @author Irving Lazu, 802-15-3736, Section 030
 * @author Fernando Rivera, 840-15-2011, Section 030
 */
public class Part2Main {

	// Default parameters if no arguments are provided
	private static int[] defParms = {10, 50, 1000, 50000, 1000, 200};
	// companies, crimes, min size, max size, increment, repetition
	
	// Main variables to be used 
	private static int telComp, 				// Number of Telephone Companies
					   crimeEv, 				// Crime Events
					   initSize, 				// Initial size for experimentations
					   finalSize, 				// Final size for experimentations
					   increment, 				// Increment of sizes
					   repetitionsPerSize;		// Number of repetitions per size

	public static void main(String[] args) {

		for (int i = 0; i < args.length; i++)
			defParms[i] = Integer.parseInt(args[i]);

		telComp = defParms[0];
		crimeEv = defParms[1];
		initSize = defParms[2];
		finalSize = defParms[3];
		increment = defParms[4];
		repetitionsPerSize = defParms[5];
		
		// ExperimentController object to run all experimentations with previous specified parameters
		ExperimentController ec = new ExperimentController(telComp, crimeEv, initSize, finalSize, increment, repetitionsPerSize); 

		/**/	
		ec.addStrategy(new StrategiesTimeCollection<Integer>(new P1AndP2("1")));
		ec.addStrategy(new StrategiesTimeCollection<Integer>(new P1AndP2("2")));
		ec.addStrategy(new StrategiesTimeCollection<Integer>(new P3("3")));
		ec.addStrategy(new StrategiesTimeCollection<Integer>(new P4("4")));
		/**/

		// Run the experiments on all the strategies added to the controller object (ec)
		try {
			ec.run();
		} catch (CloneNotSupportedException e1) {
			e1.printStackTrace();
		}   

		// Save the results for each strategy and upload to text file
		try {
			ec.saveResults();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
	}
}