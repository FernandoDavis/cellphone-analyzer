package p1MainClasses;

import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;

import dataGenerator.DataGenerator;

import java.util.ArrayList;
import java.util.Map;

import setIntersectionFinders.AbstractIntersectionFinder;
import setIntersectionFinders.GenerateDataSets;
/**
 * The StrategiesTimeCollection class implements various methods to
 * experiment with the strategies developed in part1 of this project.
 * 
 * The goal is to estimate the run time of each strategy and compare
 * them to one another using the allResults.txt file and the google
 * spreadsheet.
 * 
 * @author Irving Lazu, 802-15-3736, Section 030
 * @author Fernando Rivera, 840-15-2011, Section 030
 */
public class StrategiesTimeCollection<E> extends ArrayList<Map.Entry<Integer, Float>> {
	
	public float sum, 										// Carries the sum of estimated time taken during each repetition
				 avgTime;									// Average time taken by strategy (sum/repetitions)
	private AbstractIntersectionFinder<E> strategy;			// Current strategy being evaluated (P1, P2, P3, P4)
	AbstractMap.SimpleEntry<Integer, Float> simpleEntry;	// (n,t) coordinate where n is size and t is average time
	
	// Constructor initializing the strategy to be used for experimentation
	public StrategiesTimeCollection(AbstractIntersectionFinder<E> strategy) {
		this.strategy = strategy;
	}
	
	/**
	 * This method is used to retrieve the name of the strategy
	 * that is being used for the experiment in a run
	 * 
	 * @return name of strategy (P1, P2, P3, P4)
	 */
	public String getStrategyName() {
		return "P" + strategy.getName();
	}
	
	/**
	 * This method generates the data to be used during the
	 * experimentation process.
	 * 
	 * @param tc represents the number of telephone companies
	 * @param ce represents the number of crime events
	 * @param size
	 * @return data, a three-dimensional array containing the data for testing
	 */
	public static Integer[][][] generateData(int tc, int ce, int size) {
		DataGenerator dg = new DataGenerator(tc, ce, size);
		Integer[][][] data = (Integer[][][]) dg.generateData();  

		return data;
	}
	
	/**
	 * Method to reset the value of the sum to 0 after each
	 * repetition of the experiment.
	 */
	public void resetSum() {
		sum = 0.0f;
	}
	
	/**
	 * Method to retrieve the value of the sum of estimated times
	 * taken during repetitions of the experiment.
	 * 
	 * @return sum, sum of all times taken during experiment
	 */
	public float getSum() {
		return sum;
	}
	
	/**
	 * Increases the sum after each run by a estimated time to
	 * be averaged later on.
	 * 
	 * @param estimatedTime, time estimated by code for current run
	 */
	public void incSum(int estimatedTime) {
		sum += (float) estimatedTime;
	}
	
	/**
	 * Testing the strategy with the data generated earlier.
	 * 
	 * @param dataSet, data to be used for experimentation
	 */
	public void runTrial(Integer[][][] dataSet) {
		GenerateDataSets ds = new GenerateDataSets(dataSet);
		strategy.intersectSets(ds.generateSets(Integer.parseInt(strategy.getName())));
	}
	
	/**
	 * Method to compute the average time taken by the strategy
	 * to execute all repetitions of the trial.
	 * 
	 * @param rps, repetitions per size of data
	 * @return avgTime, average time taken
	 */
	public float computeAvgTime(int rps){
		avgTime = sum/((float) rps);
		return avgTime;
	}
}