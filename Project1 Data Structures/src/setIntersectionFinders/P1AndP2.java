package setIntersectionFinders;

import interfaces.MySet;
import mySetImplementations.Set1;
import mySetImplementations.Set2;

import java.io.FileNotFoundException;
import java.util.Iterator;

import dataGenerator.DataReader;

public class P1AndP2 extends AbstractIntersectionFinder{

	public P1AndP2(String name) {
		super(name);
	}

	@Override
	public MySet intersectSets(MySet[] t) {
		MySet T;
		if(getName().equals("1")) 
			T = new Set1();
		else
			T = new Set2();
		for(Object o: t[0])
			T.add(o);
		for(int i = 1; i<t.length; i++) {
			for(int j = 0; j<T.size(); j++) {
				Object[] TArray = T.toArray();
				if(!t[i].contains(TArray[j])) 
					T.remove(TArray[j]);
			}
		}
		return T;
	}

}
