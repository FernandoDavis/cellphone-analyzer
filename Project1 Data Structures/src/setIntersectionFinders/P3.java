package setIntersectionFinders;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;

import interfaces.MySet;
import mySetImplementations.Set2;

public class P3 extends AbstractIntersectionFinder {

	public P3(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Override
	public MySet intersectSets(MySet[] T) {
		ArrayList<Integer> allElements = new ArrayList<Integer>();
		for(int i = 0; i < T.length; i++) {
			Object[] TArray = T[i].toArray();
			for(int j = 0; j<TArray.length; j++)
				allElements.add((Integer) TArray[j]);
		}
		allElements.sort(null);
		MySet t = new Set2();  // sets in P3's solution are of type Set2
		Integer e = allElements.get(0); 
		Integer c = 1;
		for (int i=1; i < allElements.size(); i++) {
			if (allElements.get(i).equals(e)) 
				c++;
			else { 
				if (c == T.length) 
					t.add(e);    // m is as in the previous discussion
				e = allElements.get(i); 
				c = 1; 
			} 
		}
		if (c == T.length)
			t.add(allElements.get(allElements.size()-1));
		return t;
	}

}
