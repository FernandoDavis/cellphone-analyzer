package setIntersectionFinders;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import interfaces.MySet;
import mySetImplementations.Set2;

public class P4 extends AbstractIntersectionFinder {

	public P4(String name) {
		super(name);
	}

	@Override
	public MySet intersectSets(MySet[] T) {
		ArrayList<Integer> allElements = new ArrayList<Integer>();
		for(int i = 0; i < T.length; i++) {
			Object[] TArray = T[i].toArray();
			for(int j = 0; j<TArray.length; j++) {
				allElements.add((Integer) TArray[j]);
			}
		}
		HashMap<Integer, Integer> map = new HashMap<>(); 
		for (Integer e : allElements) { 
		     Integer c = map.getOrDefault(e, 0); 
		     map.put(e, c+1); 
		}
		MySet t = new Set2(); 
		for (Map.Entry<Integer, Integer> entry : map.entrySet())
		     if (entry.getValue() == T.length) 
		        t.add(entry.getKey());
		return t;
	}
}
